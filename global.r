library(data.table)
library(shiny)
library(ggplot2)
library(tools)
library(plotly)
library(shinyBS)
library(shinyjs)
library(dplyr)
library(htmlwidgets)
library(reactable)
library(stringr)

plotly.style <- list(
  plot_bgcolor = "rgba(0, 0, 0, 0)", 
  paper_bgcolor = "rgba(0, 0, 0, 0)"
)



